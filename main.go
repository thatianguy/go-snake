package main

import (
	"log"

	"github.com/hajimehoshi/ebiten/v2"

	"gitlab.com/thatianguy/go-snake/game/scenes"
)

type Game struct{}

func (g *Game) Update() error {
	currentScene.HandleInput()

	if currentScene.IsDone() {
		persistedData := currentScene.GetPersistedData()
		currentScene = gameScenes[currentScene.GetNextScene()]
		currentScene.Setup(persistedData)
	}

	if currentScene.ShouldQuit() {
		return ebiten.Termination
	}

	return nil
}

var gameScenes = scenes.GetScenes()
var currentScene = gameScenes["MENU"]

func (g *Game) Draw(screen *ebiten.Image) {
	currentScene.Draw(screen)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return 1024, 768
}

func main() {
	ebiten.SetWindowSize(1024, 768)
	ebiten.SetWindowTitle("Go Snake")

	if err := ebiten.RunGame(&Game{}); err != nil {
		log.Fatal(err)
	}
}
