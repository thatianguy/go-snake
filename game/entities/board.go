package entities

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type Board struct {
	Rows, Cols          int
	mainColor, altColor color.RGBA
	tileSize            float32
}

// Need a smarter way of handling drawing position of everything
// Have to change here multiple places, the snake, the food etc
// I think all it needs is a start X,Y ?

func NewBoard() *Board {
	return &Board{
		Rows:      12,
		Cols:      12,
		mainColor: color.RGBA{255, 100, 255, 255},
		altColor:  color.RGBA{255, 180, 255, 130},
		tileSize:  50,
	}
}

func (b *Board) Draw(screen *ebiten.Image) {
	for i := 0; i < b.Rows; i++ {
		y := float32(50 * (i + 1))

		for j := 0; j < b.Cols; j++ {
			x := float32(50 * (j + 1))

			if i%2 == 0 {
				if j%2 == 0 {
					vector.DrawFilledRect(screen, x, y, b.tileSize, b.tileSize, b.mainColor, false)
				} else {
					vector.DrawFilledRect(screen, x, y, b.tileSize, b.tileSize, b.altColor, false)
				}

			} else {
				if j%2 == 0 {
					vector.DrawFilledRect(screen, x, y, b.tileSize, b.tileSize, b.altColor, false)
				} else {
					vector.DrawFilledRect(screen, x, y, b.tileSize, b.tileSize, b.mainColor, false)
				}
			}
		}
	}
}

func (b Board) IsOutOfBounds(pos Coord) bool {
	maxX := (float32(b.Cols) * b.tileSize) + b.tileSize
	maxY := (float32(b.Rows) * b.tileSize) + b.tileSize
	if pos.X < b.tileSize || pos.Y < b.tileSize || pos.X > maxX || pos.Y > maxY {
		return true
	}

	return false
}
