package entities

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type Coord struct {
	X, Y float32
}

type Snake struct {
	Body                    []Coord
	direction, newDirection ebiten.Key
	IsColliding             bool
}

func NewSnake() *Snake {
	return &Snake{
		direction:    ebiten.KeyRight,
		newDirection: ebiten.KeyRight,
		Body: []Coord{
			{X: 75, Y: 75},
			{X: 125, Y: 75},
			{X: 175, Y: 75},
		},
		IsColliding: false}
}

func (s *Snake) UpdateDirection() {
	// Update direction of snake, disallowing turning in on yourself
	if inpututil.IsKeyJustPressed(ebiten.KeyLeft) && s.direction != ebiten.KeyRight {
		s.newDirection = ebiten.KeyLeft
	} else if inpututil.IsKeyJustPressed(ebiten.KeyRight) && s.direction != ebiten.KeyLeft {
		s.newDirection = ebiten.KeyRight
	} else if inpututil.IsKeyJustPressed(ebiten.KeyUp) && s.direction != ebiten.KeyDown {
		s.newDirection = ebiten.KeyUp
	} else if inpututil.IsKeyJustPressed(ebiten.KeyDown) && s.direction != ebiten.KeyUp {
		s.newDirection = ebiten.KeyDown
	}
}

func (s *Snake) CheckCollision(pos Coord) bool {
	for i := 0; i < len(s.Body); i++ {
		if pos.X == s.Body[i].X && pos.Y == s.Body[i].Y {
			return true
		}
	}

	return false
}

func (s *Snake) WillCollide() bool {
	newHead := s.GetHead()

	return s.CheckCollision(newHead)
}

func (s *Snake) Draw(screen *ebiten.Image) {
	for i := 0; i < len(s.Body); i++ {
		vector.DrawFilledCircle(screen, s.Body[i].X, s.Body[i].Y, 20, color.Black, false)
	}
}

func (s *Snake) Update() {
	head := s.GetHead()
	newHead := head
	s.direction = s.newDirection
	switch s.direction {
	case ebiten.KeyRight:
		newHead.X += 50
	case ebiten.KeyLeft:
		newHead.X -= 50
	case ebiten.KeyUp:
		newHead.Y -= 50
	default:
		newHead.Y += 50
	}

	// Check self collision
	for i := 0; i < len(s.Body); i++ {
		if s.Body[i].X == newHead.X && s.Body[i].Y == newHead.Y {
			s.IsColliding = true
		}
	}

	s.Body = append(s.Body[1:], newHead)
}

func (s *Snake) Grow() {
	tail := s.Body[0]
	s.Body = append([]Coord{tail}, s.Body...)
}

func (s *Snake) GetHead() Coord {
	return s.Body[len(s.Body)-1]
}
