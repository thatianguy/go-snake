package entities

import (
	"image/color"
	"math/rand"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type Food struct {
	X, Y float32
}

func NewFood() *Food {
	return &Food{X: 0, Y: 0}
}

func (f Food) Draw(screen *ebiten.Image) {
	vector.DrawFilledCircle(screen, f.X, f.Y, 10, color.RGBA{245, 84, 66, 255}, false)
}

func (f *Food) GetNewPosition(snakeBody []Coord, boardRows, boardCols int) {
	found := false
	for !found {
		randX := rand.Intn(boardCols-1) + 1
		randY := rand.Intn(boardRows-1) + 1
		centreX := float32((50 * randX) + 25)
		centreY := float32((50 * randY) + 25)

		found = !checkCollisionWithCoords(centreX, centreY, snakeBody)

		if found {
			f.X = centreX
			f.Y = centreY
		}
	}
}

func checkCollisionWithCoords(x, y float32, coords []Coord) bool {
	for i := 0; i < len(coords); i++ {
		if x == coords[i].X && y == coords[i].Y {
			return true
		}
	}

	return false
}
