package scenes

import (
	"fmt"
	"image/color"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/text"
	"gitlab.com/thatianguy/go-snake/game/entities"
	"golang.org/x/image/font"
)

type GameplayScene struct {
	NextScene      string
	Font           font.Face
	Done, Quitting bool
	Timer          time.Time
	Score          int
	Snake          *entities.Snake
	Food           *entities.Food
	Board          *entities.Board
}

// Refactoring:
//	- Re-using a menu scene with options passed in for both main menu and game over
//	- Passing final score to game over screen
//	- Input handling on the menu
//	- Passing Coord instead of separate x and y
//	- Positioning of elements

// Tomorrow:
// - Sounds (crappy, self recorded sounds for fun)
// - Retain high score between games
// Monday/Tuesday:
// - Animations?

func NewGameplayScene(gameFont font.Face) *GameplayScene {
	snake := entities.NewSnake()
	food := entities.NewFood()
	board := entities.NewBoard()
	food.GetNewPosition(snake.Body, board.Rows, board.Cols)

	return &GameplayScene{
		NextScene: "GAME_OVER",
		Font:      gameFont,
		Done:      false,
		Quitting:  false,
		Timer:     time.Now(),
		Score:     0,
		Snake:     snake,
		Food:      food,
		Board:     board,
	}
}

func (s *GameplayScene) Setup(persistedData map[string]any) {
	s.Done = false
	s.Quitting = false
	s.Score = 0
	s.Snake = entities.NewSnake()
	s.Food.GetNewPosition(s.Snake.Body, s.Board.Rows, s.Board.Cols)
}

func (s GameplayScene) GetNextScene() string {
	return s.NextScene
}

func (s GameplayScene) IsDone() bool {
	return s.Done
}

func (s GameplayScene) ShouldQuit() bool {
	return s.Quitting
}

func (s *GameplayScene) HandleInput() {
	if inpututil.IsKeyJustReleased(ebiten.KeyEscape) {
		s.Quitting = true
	}

	difficultyValue := (400 - (s.Score * 15))
	interval := time.Millisecond * time.Duration(difficultyValue)

	s.Snake.UpdateDirection()

	if time.Since(s.Timer) >= interval {
		s.Snake.Update()

		if s.Snake.IsColliding {
			s.Done = true
		}

		snakeHead := s.Snake.GetHead()

		// Check out of bounds
		if s.Board.IsOutOfBounds(snakeHead) {
			s.Done = true
		}

		if snakeHead.X == s.Food.X && snakeHead.Y == s.Food.Y {
			// Eaten food, grow the snake
			s.Score += 1
			s.Snake.Grow()
			s.Food.GetNewPosition(s.Snake.Body, s.Board.Rows, s.Board.Cols)
		}

		s.Timer = time.Now()

	}
}

func (s GameplayScene) Draw(screen *ebiten.Image) {
	scoreText := fmt.Sprint("Score: ", s.Score)
	text.Draw(screen, scoreText, s.Font, 50, 50, color.White)

	s.Board.Draw(screen)
	s.Snake.Draw(screen)
	s.Food.Draw(screen)
}

func (s GameplayScene) GetPersistedData() map[string]any {
	return map[string]any{"SCORE": s.Score}
}
