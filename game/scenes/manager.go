package scenes

import (
	"os"

	"github.com/hajimehoshi/ebiten/v2"
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
)

type Scene interface {
	GetNextScene() string
	IsDone() bool
	ShouldQuit() bool
	Draw(screen *ebiten.Image)
	HandleInput()
	Setup(map[string]any)
	GetPersistedData() map[string]any
}

func GetScenes() map[string]Scene {
	gameFont := loadGameFont()

	menuScene := NewMenuScene(gameFont, []string{"Play", "Test", "Quit"}, "GAMEPLAY")
	gameplayScene := NewGameplayScene(gameFont)
	gameOverScene := NewMenuScene(gameFont, []string{"Play Again", "Quit"}, "GAMEPLAY")

	scenes := map[string]Scene{
		"MENU":      menuScene,
		"GAMEPLAY":  gameplayScene,
		"GAME_OVER": gameOverScene,
	}

	return scenes
}

// TODO: Will need other sized fonts
func loadGameFont() font.Face {
	myFont, err := os.ReadFile("assets/fonts/SuperComic.ttf")
	if err != nil {
		panic(err)
	}

	tt, err := opentype.Parse(myFont)
	if err != nil {
		panic(err)
	}

	gameFont, err := opentype.NewFace(tt, &opentype.FaceOptions{Size: 36, DPI: 72, Hinting: font.HintingFull})
	if err != nil {
		panic(err)
	}

	return gameFont
}
