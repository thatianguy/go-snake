package scenes

import (
	"fmt"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/text"
	"golang.org/x/image/font"
)

type MenuScene struct {
	NextScene      string
	Options        []string
	Font           font.Face
	SelectedIndex  int
	Done, Quitting bool
	PersistedData  map[string]any
}

func NewMenuScene(gameFont font.Face, options []string, nextScene string) *MenuScene {
	return &MenuScene{
		NextScene:     nextScene,
		Options:       options,
		Font:          gameFont,
		SelectedIndex: 0,
		Done:          false,
		Quitting:      false,
	}
}

func (s *MenuScene) Setup(persistedData map[string]any) {
	s.PersistedData = persistedData
	s.Done = false
	s.Quitting = false
	s.SelectedIndex = 0
}

func (s MenuScene) GetNextScene() string {
	return s.NextScene
}

func (s MenuScene) IsDone() bool {
	return s.Done
}

func (s MenuScene) ShouldQuit() bool {
	return s.Quitting
}

func (s *MenuScene) HandleInput() {
	// TODO: This isn't good - if you hold down the key, you'd expect to slowly cycle through options?
	//       Would probably require some custom timer for interval check to make prevent too frequent updates
	//		 when using IsKeyPressed
	if inpututil.IsKeyJustReleased(ebiten.KeyUp) {
		if s.SelectedIndex == 0 {
			s.SelectedIndex = len(s.Options) - 1
		} else {
			s.SelectedIndex -= 1
		}
	}

	if inpututil.IsKeyJustReleased(ebiten.KeyDown) {
		if s.SelectedIndex == len(s.Options)-1 {
			s.SelectedIndex = 0
		} else {
			s.SelectedIndex += 1
		}
	}

	if inpututil.IsKeyJustReleased(ebiten.KeyEnter) {
		if s.SelectedIndex == 0 {
			s.Done = true
		}
		if s.SelectedIndex == len(s.Options)-1 {
			s.Quitting = true
		}
	}
}

func (s MenuScene) Draw(screen *ebiten.Image) {
	if val, ok := s.PersistedData["SCORE"]; ok {
		textColor := color.RGBA{R: 239, G: 150, B: 226, A: 0}
		text.Draw(screen, fmt.Sprint("Your final score was ", val), s.Font, 50, 50, textColor)
	} else {
		textColor := color.RGBA{R: 239, G: 150, B: 226, A: 0}
		text.Draw(screen, "Go Snek", s.Font, 400, 80, textColor)
	}

	startY := 150
	for i := 0; i < len(s.Options); i++ {
		textColor := color.RGBA{R: 255, G: 255, B: 255, A: 255}

		if i == s.SelectedIndex {
			textColor = color.RGBA{R: 239, G: 40, B: 226, A: 255}
		}

		text.Draw(screen, s.Options[i], s.Font, 150, startY, textColor)
		startY += 70
	}
}

func (s MenuScene) GetPersistedData() map[string]any {
	return map[string]any{}
}
